package pl.application.downloader.resources;

import pl.application.downloader.DataStore;
import pl.application.downloader.model.Website;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * REST resource for managing websites.
 *
 * @author Paweł Wieraszka
 */
@Path("/websites")
public class DownloadsResource {

    /**
     * Injected application wide bean.
     */
    @Inject
    private DataStore store;

    /**
     * Gets all websites to download in JSON format.
     *
     * @return All websites.
     */
    @Path("/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Website> getWebsites() {
        return store.getWebsites();
    }

    /**
     * Gets website with given id in JSON format.
     *
     * @param id Id of website to be shown.
     * @return website with chosen id.
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Website getWebsite(@PathParam("id") int id) {
        return store.getWebsite(id);
    }

    /**
     * Consumes JSON and adds new website to download.
     *
     * @param url Url adress of website that should be downloaded.
     */
    @Path("/")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addWebsite(String url) {
        store.addWebsite(url);
    }
}
