package pl.application.downloader;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import pl.application.downloader.model.Website;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Downloads content of websites. Works background.
 *
 * @author Paweł Wieraszka
 */
@Singleton
public class DownloaderBean {

    private static final Logger log = Logger.getLogger(DownloaderBean.class.getName());

    @Inject
    private DataStore store;

    /**
     * Realizes the process of downloading a website's content in the background.
     */
    @Schedule(second = "*/10", minute = "*", hour = "*", persistent = false)
    public void downloadContent() {

        for(Website website : store.getWebsitesToDownload()) {
            // Creates an instance of HttpClient.
            HttpClient client = new HttpClient();
            // Creates a method instance.
            GetMethod method = new GetMethod(website.getUrl());
            // Sets temporary website's status.
            website.setDownloaded(Website.Downloaded.IN_PROGRESS);
            // Retries three times if the connection didn't go through.
            method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                    new DefaultHttpMethodRetryHandler(3,false));

            try {
                //Executes the method.
                int statusCode = client.executeMethod(method);
                if (statusCode != HttpStatus.SC_OK) {
                    log.log(Level.WARNING, "Method failed: " + method.getStatusLine());
                }
                // Reads the response body.
                byte[] responseBody = method.getResponseBody();
                // Writes response body as string.
                website.setContent(new String(responseBody));
                // Changes website's status to downloaded.
                website.setDownloaded(Website.Downloaded.YES);

            } catch (HttpException e) {
                log.log(Level.WARNING, "Protocol violation: " + e.getMessage(), e);

            } catch (IOException e) {
                log.log(Level.WARNING, "Transport error: " + e.getMessage(), e);
                e.printStackTrace();

            } finally {
                // Releases the connection.
                method.releaseConnection();
            }
        }
    }
}
