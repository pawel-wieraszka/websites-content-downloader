package pl.application.downloader;

import pl.application.downloader.resources.DownloadsResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * REST context configuration.
 *
 * @author Paweł Wieraszka
 */
@ApplicationPath("/api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        return new HashSet<>(Arrays.asList(DownloadsResource.class));
    }
}
