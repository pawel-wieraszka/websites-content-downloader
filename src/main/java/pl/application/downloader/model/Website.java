package pl.application.downloader.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.lang.String;

/**
 * Class of website.
 *
 * @author Paweł Wieraszka
 */
@Entity
@Table(name = "websites")
public class Website implements Serializable {

    public enum Downloaded {
        YES,
        IN_PROGRESS,
        NO;
    }

    /**
     * Id of website.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Website's url.
     */
    @Column(unique = true)
    private String url;

    /**
     * Website's content.
     */
    @Column(columnDefinition = "MEDIUMBLOB")
    private String content;

    /**
     * Website's status of download.
     */
    @Column
    @Enumerated(value = EnumType.STRING)
    private Downloaded downloaded;

    public Website() {
    }

    public Website(String url) {
        this.url = url;
        this.content = "";
        this.downloaded = Downloaded.NO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Downloaded getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Downloaded downloaded) {
        this.downloaded = downloaded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Website website = (Website) o;
        return id == website.id &&
                Objects.equals(url, website.url) &&
                Objects.equals(content, website.content) &&
                downloaded == website.downloaded;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, url, content, downloaded);
    }
}
