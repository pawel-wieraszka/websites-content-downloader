package pl.application.downloader;

import pl.application.downloader.model.Website;
import pl.application.downloader.model.Website_;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Performs queries and operations on the database.
 *
 * @author Paweł Wieraszka
 */
@Stateless
public class DataStore {

    @PersistenceContext
    private EntityManager em;

    /**
     * Performs query that returns all websites from database.
     *
     * @return All websites.
     */
    public List<Website> getWebsites() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Website> query = cb.createQuery(Website.class);
        query.from(Website.class);
        return em.createQuery(query).getResultList();
    }

    /**
     * Returns websites which content is not downloaded.
     *
     * @return All websites that aren't downloaded.
     */
    public List<Website> getWebsitesToDownload() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Website> query = cb.createQuery(Website.class);
        Root<Website> root = query.from(Website.class);
        query.select(root).where(cb.equal(root.get(Website_.downloaded), Website.Downloaded.NO));
        return em.createQuery(query).getResultList();
    }

    /**
     * Returns website by id.
     *
     * @param id Id of website that should be returned.
     * @return Website with given id.
     */
    public Website getWebsite(int id) {
        return em.find(Website.class, id);
    }

    /**
     * Adds new website into database.
     *
     * @param url Url adress of website that should be downloaded.
     */
    public void addWebsite(String url) {
        em.persist(new Website(url));
    }
}
